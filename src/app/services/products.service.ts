import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { Product } from "app/models/product.model";
import "rxjs/add/operator/map";
import { Observable } from "rxjs";
import { CacheBase } from "./caching.service";

let count = 0;

@Injectable()
export class ProductsDataService extends CacheBase {
  private products: Observable<Product[]>;
  categories:any;
  selectedList:any;
  foodList:any;
  items:any;
  id:any;

  public constructor(private http: Http) {
    super();
 
   
  }

  public all(): Observable<Product[]> {
    return this.cache<Product[]>(() => this.products,
                                 (val: Observable<Product[]>) => this.products = val,
                                 () => this.http
                                           .get("./assets/products_alter.json")
                                           .map((response) => response.json()
                                                                      .map((item) => {
                                                                        let model = new Product();
                                                                        model.updateFrom(item);
                                                                        return model;
                                                                      })));

  }

 

    
    

}

