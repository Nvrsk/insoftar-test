

export class Product {
  public quantity: string;
  public price: number;
  public available: boolean;
  public sublevel_id: any;
  public name: string;
  public serial:string;
  public icon: string;
  public id: any;
  

  public updateFrom(src: Product): void {

    this.quantity = src.quantity;
    this.price = src.price;
    this.available = src.available;
    this.sublevel_id = src.sublevel_id;
    this.name = src.name;
    this.serial = src.serial;
    this.icon = src.icon;
    this.id = src.id;
    
 
  }


}
