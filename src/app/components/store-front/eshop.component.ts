import { ChangeDetectionStrategy, Component, OnInit } from "@angular/core";
import { Product } from "app/models/product.model";
import { ShoppingCart } from "app/models/shopping-cart.model";
import { ProductsDataService } from "app/services/products.service";
import { ShoppingCartService } from "app/services/shopping-cart.service";
import { Observable ,  Observer } from "rxjs";
import { Http } from '@angular/http';
import { HttpModule} from '@angular/http';
import 'rxjs/add/operator/map';


@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "app-eshop",
  styleUrls: ["./eshop.component.scss"],
  templateUrl: "./eshop.component.html"
})
export class EshopComponent implements OnInit {
  public Id:any;
  public productList:any;
  public overview:any;
  public categories:any;
  public foodList:any;
  public product:any;
  public stockid:any;
  public lists:any;
  public stock:any;
  public outstock:any;
  public products: Observable<Product[]>;
  order: string = 'price';
  

  public constructor(private productsService: ProductsDataService,
                     private shoppingCartService: ShoppingCartService, private http:Http) {
                       this.getCategory();
                  
                      
  }

  public addProductToCart(product: Product): void {
    this.shoppingCartService.addItem(product, 1);
  }

  public removeProductFromCart(product: Product): void {
    this.shoppingCartService.addItem(product, -1);
  }

  public productInCart(product: Product): boolean {
    return Observable.create((obs: Observer<boolean>) => {
      const sub = this.shoppingCartService
                      .get()
                      .subscribe((cart) => {
                        obs.next(cart.items.some((i) => i.productId === product.id));
                        obs.complete();
                      });
      sub.unsubscribe();
    });
  }

  public ngOnInit(): void {
    this.products = this.productsService.all();

  }

  selectedProduct: Product;
  
  onSelect(product: Product): void {
    this.selectedProduct = product;
    this.outstock = this.selectedProduct.available == false;
  }




  filterClassDrinks(product: Product) {   
    return product.sublevel_id <= 3;  
  }
  filterDrinksAvailable(product: Product){
    return product.sublevel_id <= 3 && product.available == true;
  }
  filterClassBreakfast(product: Product) { 
    return product.sublevel_id >= 4 && product.sublevel_id <=7 ;
  }
  filterClassLunch(product: Product) { 
    return product.sublevel_id >= 8 && product.sublevel_id <=10 ;
  }
  filterClassWines(product: Product) { 
    return product.sublevel_id >= 11 && product.sublevel_id <=13 ;
  }


  getCategory(){
    this.categories = '../../assets/categories.json' ;

    this.http.get(this.categories)
    .map(res=>{
     return res.json()
     
     
   })
   .subscribe(listData=>{
    this.lists = listData;
    console.log(this.lists[0].sublevels[0].sublevels[0].id);

    
   }) }

}


